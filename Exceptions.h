/* 
 * File:   Exceptions.h
 * Author: martin
 *
 * Created on 7 décembre 2014, 19:08
 */

#ifndef EXCEPTIONS_H
#define	EXCEPTIONS_H
#include <exception>
#include <string>
using namespace std;

// Classe mère de toutes les exceptions de l'interpréteur
class InterpreteurException : public exception {
public:
    const char * what() const throw() {
        return "Exception Interpreteur";
    }
};

class FichierException : public InterpreteurException {
public:
    const char * what() const throw() {
        return "Ouverture Fichier Impossible";
    }
};

class SyntaxeException : public InterpreteurException {
public:
    SyntaxeException(const char * message = NULL) : m_message(message) {}
    const char * what() const throw() {
        return m_message;
    }
private :
    const char* m_message;
};


class IndefiniException : public InterpreteurException {
public:
    const char * what() const throw() {
        return "Valeur Indéfinie";
    }
};


class DivParZeroException : public InterpreteurException {
public:
    const char * what() const throw() {
        return "Division par 0";
    }
};

class OperationInterditeException : public InterpreteurException {
public:
    const char * what() const throw() {
        return "Operation Interdite sur un noeud";
    }
};

class ParametresException : public InterpreteurException {
public:
    const char * what() const throw() {
        return "Nombre de parametres invalides lors d'un appel de fonction";
    }
};

class AppelException : public InterpreteurException {
public:
    const char * what() const throw() {
        return "Fonction inconnue lors d'un appel de fonction";
    }
};

#endif	/* EXCEPTIONS_H */

