/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "Valeur.h"

#include <iostream>

Valeur::Valeur() :
m_valeur() {
}

Valeur::Valeur(int valeur) :
m_valeur(std::to_string(valeur)) {
}

Valeur::Valeur(const std::string &valeur) :
m_valeur(valeur) {
}

Valeur Valeur::operator +(Valeur &valeur) {
    try {
        return int(*this) + int(valeur);
    } catch(ValeurException &e) {
        return this->m_valeur + valeur.m_valeur;
    }
}

Valeur Valeur::operator -(Valeur &valeur) {
    return int(*this) - int(valeur);
}

Valeur Valeur::operator *(Valeur &valeur) {
    return int(*this) * int(valeur);
}

Valeur Valeur::operator /(Valeur &valeur) {
    return int(*this) / int(valeur);
}

Valeur Valeur::operator ||(Valeur &valeur) {
    return int(*this) || int(valeur);
}

Valeur Valeur::operator &&(Valeur &valeur) {
    return int(*this) && int(valeur);
}

Valeur Valeur::operator !=(Valeur &valeur) {
    return this->m_valeur != valeur.m_valeur;
}

Valeur Valeur::operator ==(Valeur &valeur) {
    return this->m_valeur == valeur.m_valeur;
}

Valeur Valeur::operator ==(const Valeur valeur) const {
    return this->m_valeur == valeur.m_valeur;
}

Valeur Valeur::operator <=(Valeur &valeur) {
    return int(*this) <= int(valeur);
}

Valeur Valeur::operator >=(Valeur &valeur) {
    return int(*this) >= int(valeur);
}

Valeur Valeur::operator <(Valeur &valeur) {
    return int(*this) < int(valeur);
}

Valeur Valeur::operator >(Valeur &valeur) {
    return int(*this) > int(valeur);
}

Valeur::operator int() throw (ValeurException) {
    try {
        return std::stoi(this->m_valeur);
    } catch(...) {
        throw ValeurException();
    }
}

std::ostream &operator <<(std::ostream &out, const Valeur &valeur) {
    return out << valeur.m_valeur;
}

std::istream &operator >>(std::istream &input, Valeur &valeur) {
    return input >> valeur.m_valeur;
}