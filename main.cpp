#include <iostream>
#include "Interpreteur.h"
#include "Exceptions.h"
#include "Execution.h"

using namespace std;

int main(int argc, char* argv[]) {
    string nomFich;
    bool tradCPP = false;
    string nomFichCPP;

    for(int i = 1; i < argc; i++) {
        if(argv[i][0] == '-') {
            if(std::string(argv[i]) == "--cpp") {
                tradCPP = true;
            } else {
                cout << "Argument inconnu " << argv[i] << endl;
            }
        } else {
            nomFich = argv[i];
        }
    }

    if(nomFich.empty()) {
        cout << "Usage : " << argv[0] << " nom_fichier_source" << endl << endl;
        cout << "Entrez le nom du fichier que voulez-vous interpréter : ";
        getline(cin, nomFich);
    }

    ifstream fichier(nomFich);
    
    try {
        Interpreteur interpreteur(fichier);
        interpreteur.analyse();
        
        if(tradCPP) {
            nomFichCPP = nomFich.substr(0, nomFich.find('.')) + ".cpp"; 
            ofstream fichCPP(nomFichCPP);
            
            cout << nomFichCPP << endl;
            
            interpreteur.traduitEnCPP(fichCPP);
            
            system(("g++ -o TP5TraductionCPP " + nomFichCPP + " ValeurCPP.cpp -std=c++11").c_str());
        } else {
            interpreteur.executer("principale");
        }
        
    } catch (InterpreteurException & e) {
        cout << e.what() << endl;
    }
    
    return 0;
}
