/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Valeur.h
 * Author: gaisnea
 *
 * Created on 3 novembre 2017, 15:12
 */

#ifndef VALEUR_H
#define VALEUR_H

#include <string>
#include <exception>
#include <iostream>

class ValeurException : public std::exception {
public:
    const char *what() const throw() {
        return "Conversion Impossible";
    }
};

class Valeur {
    friend class Table;
    std::string m_valeur;
    
public:
    Valeur();
    Valeur(int valeur);
    Valeur(const std::string &valeur);
    
    Valeur operator +(Valeur &valeur);
    Valeur operator -(Valeur &valeur);
    Valeur operator *(Valeur &valeur);
    Valeur operator /(Valeur &valeur);
    Valeur operator ||(Valeur &valeur);
    Valeur operator &&(Valeur &valeur);
    Valeur operator !=(Valeur &valeur);
    Valeur operator ==(Valeur &valeur);
    Valeur operator ==(const Valeur valeur) const;
    Valeur operator <=(Valeur &valeur);
    Valeur operator >=(Valeur &valeur);
    Valeur operator <(Valeur &valeur);
    Valeur operator >(Valeur &valeur);
    operator int() throw (ValeurException);
    
    friend std::ostream &operator <<(std::ostream &out, const Valeur &valeur);
    friend std::istream &operator >>(std::istream &input, Valeur &valeur);
};

std::ostream &operator <<(std::ostream &out, const Valeur &valeur);
std::istream &operator >>(std::istream &input, Valeur &valeur);

#endif /* VALEUR_H */

