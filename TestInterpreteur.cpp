/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   TestInterpreteur.cpp
 * Author: valentin
 *
 * Created on 6 nov. 2017, 15:43:03
 */

#include "TestInterpreteur.h"
#include "Interpreteur.h"


CPPUNIT_TEST_SUITE_REGISTRATION(TestInterpreteur);

TestInterpreteur::TestInterpreteur() {
}

TestInterpreteur::~TestInterpreteur() {
}

void TestInterpreteur::setUp() {
}

void TestInterpreteur::tearDown() {
}

void TestInterpreteur::testAnalyse() {
    std::ifstream fichier = std::ifstream("testSi.txt");
    Interpreteur interpreteur = Interpreteur(fichier);
    CPPUNIT_ASSERT_NO_THROW_MESSAGE("Analyse effectué sans problème", interpreteur.analyse());
    
    std::ifstream fichier2 = std::ifstream("testSyntaxeIncorrecte.txt");
    Interpreteur interpreteur2 = Interpreteur(fichier2);
    CPPUNIT_ASSERT_THROW_MESSAGE("Analyse effectué sans problème", interpreteur2.analyse(), InterpreteurException);

}

void TestInterpreteur::testExecuter() {
    std::ifstream fichier = std::ifstream("testSi.txt");
    Interpreteur interpreteur = Interpreteur(fichier);
    interpreteur.analyse();
    Table &table = interpreteur.executer("principale");
    
    NoeudValue &test1 = table["test1"];
    NoeudValue &test2 = table["test2"];
    
    const Valeur obtenu1 = test1.executer();
    const Valeur obtenu2 = test2.executer();
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Test1 égal à 2", obtenu1, Valeur(2));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Test2 égal à 1", obtenu2, Valeur(1));
}


