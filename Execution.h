
/* 
 * File:   Execution.h
 * Author: valentin
 *
 * Created on 4 novembre 2017, 18:42
 */

#ifndef EXECUTION_H
#define EXECUTION_H

#include "ArbreAbstrait.h"

#include <string>
#include <map>
#include <vector>
#include <stack>

class Table {
    std::string m_nom;
    std::map<std::string, NoeudValue> m_table;
    
public:
    Table(const std::string &nom);
    
    /*
     *  Chaque table est définie avec le nom de la procedure qui l'appelle,
     * elles sont composées de NoeudValue et remplacent la TableSymboles afin
     * de faciliter la création de procedures. Ses symboles sont soient des
     * variables, soit des symboles "cachés". Ceux cachés commenceront avec le
     * caractère ":" car c'est un mot clé et on ne risquera pas de collision
     * avec les nom de variables de cette façon
     */
    
    NoeudValue &operator[](const std::string &nom);
    // Remplacement pour chercheAjoute
    
    void definir(std::vector<Noeud *> parametres);
    // Rempli dans l'ordre les symboles non cachés par les parametres
    
    void indefinir();
    // Remet chacun de ses NoeudValue non caché a non defini
    
    const std::string &getNom() const;
    
    void afficher();
    
    std::string parametresCPP() const;
    const std::string valeurCPP(const NoeudValue *valeur) const;
    // permet de traduire un NoeudValue en CPP qu'il soit une variable ou une constante
};

class Pile {
    /*
     *  Une pile sous forme de singleton
     * résoud le problème d'accès a une table
     * avec les procedures
     */
    
    static Pile *instance;
    static void deinit();
    Pile();
    std::stack<Table *> m_stack;
public:
    static Pile &get();
    
    Table *sommet();
    void empiler(Table *table);
    void depiler();
    NoeudValue *operator[](const std::string &nom);
    // obtenir la variable nommée nom dans la table courante (le sommet)
};

////////////////////////////////////////////////////////////////////////////////

class NoeudProcedure : public Noeud {
    Table m_table;
    NoeudSeqInst *m_sequence;

public:
    NoeudProcedure(const std::string &nom);
    ~NoeudProcedure();
    
    void ajoute(Noeud* seq) override;
    Valeur executer() override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;

    const std::string &getNom() const;
    Table &getTable();
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstAppel : public Noeud {
    NoeudProcedure *m_proc;
    std::vector<Noeud *> m_parametres;

public:
    NoeudInstAppel(NoeudProcedure *proc);
    ~NoeudInstAppel();
    
    void ajoute(Noeud* param) override;
    Valeur executer() override;
    void traduitEnCPP(std::ostream &sortie, unsigned int indentation) const override;
};

#endif /* EXECUTION_H */

