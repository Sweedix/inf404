/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ValeurCPP.h
 * Author: valentin
 *
 * Created on 5 novembre 2017, 21:01
 */

#ifndef VALEURCPP_H
#define VALEURCPP_H

#include <string>
#include <exception>
#include <iostream>

class ValeurCPPException : public std::exception {
public:
    const char *what() const throw() {
        return "Conversion Impossible";
    }
};

class ValeurCPP {
    std::string m_valeur;
    
public:
    ValeurCPP(std::string valeur);
    ValeurCPP(int valeur);
    
    void operator =(ValeurCPP valeur);
    bool operator ==(ValeurCPP valeur);
    bool operator !=(ValeurCPP valeur);
    bool operator <=(ValeurCPP valeur);
    bool operator >=(ValeurCPP valeur);
    bool operator <(ValeurCPP valeur);
    bool operator >(ValeurCPP valeur);
    bool operator &&(ValeurCPP valeur);
    bool operator ||(ValeurCPP valeur);
    
    ValeurCPP operator +(ValeurCPP valeur);
    ValeurCPP operator -(ValeurCPP valeur);
    ValeurCPP operator *(ValeurCPP valeur);
    ValeurCPP operator /(ValeurCPP valeur);
    
    operator int() throw (ValeurCPPException);
    
    friend std::ostream &operator <<(std::ostream &out, const ValeurCPP &valeur);
    friend std::istream &operator >>(std::istream &input, ValeurCPP &valeur);
};

std::ostream &operator <<(std::ostream &out, const ValeurCPP &valeur);
std::istream &operator >>(std::istream &input, ValeurCPP &valeur);

#endif /* VALEURCPP_H */

