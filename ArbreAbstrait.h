#ifndef ARBREABSTRAIT_H
#define ARBREABSTRAIT_H

// Contient toutes les déclarations de classes nécessaires
//  pour représenter l'arbre abstrait

#include <vector>
#include <iostream>
#include <iomanip>

#include "Valeur.h"
#include "Symbole.h"
#include "Exceptions.h"


class Indente {
    unsigned int indentation;
public:
    Indente(unsigned int indent = 0);
    void indenter(std::ostream &out) const;
};

std::ostream &operator <<(std::ostream &out, Indente indent);

////////////////////////////////////////////////////////////////////////////////
class Noeud {
// Classe abstraite dont dériveront toutes les classes servant à représenter l'arbre abstrait
// Remarque : la classe ne contient aucun constructeur
public:
    virtual Valeur executer() = 0 ; // Méthode pure (non implémentée) qui rend la classe abstraite
    virtual void ajoute(Noeud* instruction);
    virtual ~Noeud();
    virtual void traduitEnCPP(std::ostream &sortie, unsigned int indentation = 0) const = 0;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudValue : public Noeud {
    friend class Table;
    static int nextId;
    int m_id;
    Valeur m_valeur;
    bool m_defini;

public:
    NoeudValue();
    ~NoeudValue();
    bool estDefini();
    void indefinir();
    Valeur executer() override;
    void operator =(Valeur valeur);
    void traduitEnCPP(std::ostream &sortie, unsigned int indentation = 0) const override;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudSeqInst : public Noeud {
    vector<Noeud *> m_instructions;
    
public:
    NoeudSeqInst();
   ~NoeudSeqInst();
   Valeur executer() override;
   void ajoute(Noeud* instruction) override;
   void traduitEnCPP(std::ostream &sortie, unsigned int indentation) const override;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudAffectation : public Noeud {
    NoeudValue *m_variable;
    Noeud* m_expression;

public:
     NoeudAffectation(NoeudValue *variable, Noeud *expression);
    ~NoeudAffectation();
    Valeur executer() override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;
};

////////////////////////////////////////////////////////////////////////////////

class NoeudOperateurBinaire : public Noeud {
    Symbole m_operateur;
    Noeud*  m_operandeGauche;
    Noeud*  m_operandeDroit;
    
public:
    NoeudOperateurBinaire(Symbole operateur, Noeud* operandeGauche, Noeud* operandeDroit);
   ~NoeudOperateurBinaire();
    Valeur executer() override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstSi : public Noeud {
    Noeud*  m_condition;
    Noeud*  m_sequence;
    
public:
    NoeudInstSi(Noeud* condition, Noeud* sequence);
   ~NoeudInstSi();
    Valeur executer() override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstSiRiche : public Noeud {
    vector<Noeud*>  m_instsSi;
    
public:
    NoeudInstSiRiche();
   ~NoeudInstSiRiche();
    Valeur executer() override;
    void ajoute(Noeud* instSi) override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstTantQue : public Noeud {
    Noeud* m_condition;
    Noeud* m_sequence;

public :
    NoeudInstTantQue(Noeud* expression, Noeud* sequence);
    ~NoeudInstTantQue();
    Valeur executer() override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstRepeter : public Noeud {
    Noeud* m_sequence;
    Noeud* m_condition;

public:
    NoeudInstRepeter(Noeud* condition, Noeud* sequence);
    ~NoeudInstRepeter();
    Valeur executer() override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstPour : public Noeud {
    Noeud* m_expression;
    Noeud* m_sequence;
    Noeud* m_affect1;
    Noeud* m_affect2;

public:
    NoeudInstPour(Noeud *affect1, Noeud* expression, Noeud *affect2, Noeud* sequence);
    ~NoeudInstPour();
    Valeur executer() override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstEcrire : public Noeud {
    vector<Noeud *> m_expressions;
    
public:
    NoeudInstEcrire();
    ~NoeudInstEcrire();
    Valeur executer() override;
    void ajoute(Noeud* expression) override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstLire : public Noeud {
    vector<Noeud *> m_variables;
public:
    NoeudInstLire();
    ~NoeudInstLire();
    Valeur executer() override;
    void ajoute(Noeud* variable) override;
    void traduitEnCPP(std::ostream & sortie, unsigned int indentation) const override;
    
};

#endif /* ARBREABSTRAIT_H */
