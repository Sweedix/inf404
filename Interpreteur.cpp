#include "Interpreteur.h"
#include "Execution.h"
#include "ArbreAbstrait.h"
using namespace std;

Interpreteur::Interpreteur(ifstream & fichier) :
m_lecteur(fichier), m_procedures(), analyseValide(true) {
}

Interpreteur::~Interpreteur() {
}

void Interpreteur::analyse() throw (InterpreteurException) {
    while(this->m_lecteur.getSymbole() != "<FINDEFICHIER>") {
        NoeudProcedure *proc = this->procedure();
        if(proc != nullptr) {
            this->m_procedures.push_back(proc);
            //proc->getTable().afficher();
        }
    }
    
    if(!this->analyseValide) {
        this->m_procedures.clear();
        throw InterpreteurException();
    }
}

Table &Interpreteur::executer(const std::string& named) throw (InterpreteurException) {
    Pile &pile = Pile::get();
    NoeudProcedure *proc = this->getProcedure(named);
    if (proc == nullptr)
        throw InterpreteurException();
    
    pile.empiler(&proc->getTable());
    proc->executer();

    pile.sommet()->afficher();

    pile.depiler();
    
    return proc->getTable();
}

NoeudProcedure *Interpreteur::getProcedure(const std::string &named) {

    for(NoeudProcedure *proc : this->m_procedures) {
        if(proc->getNom() == named)
            return proc;
    }
    
    return nullptr;
}

void Interpreteur::tester(const string & symboleAttendu) throw (SyntaxeException) {
  // Teste si le symbole courant est égal au symboleAttendu... Si non, lève une exception
  static char messageWhat[256];
  if (m_lecteur.getSymbole() != symboleAttendu) {
    sprintf(messageWhat,
            "Ligne %d, Colonne %d - Erreur de syntaxe - Symbole attendu : %s - Symbole trouvé : %s",
            m_lecteur.getLigne(), m_lecteur.getColonne(),
            symboleAttendu.c_str(), m_lecteur.getSymbole().getChaine().c_str());
    this->analyseValide = false;
    throw SyntaxeException(messageWhat);
  }
}

void Interpreteur::avancer() {
    this->m_lecteur.avancer();
}

void Interpreteur::testerEtAvancer(const string & symboleAttendu) throw (SyntaxeException) {
  // Teste si le symbole courant est égal au symboleAttendu... Si oui, avance, Sinon, lève une exception
  this->tester(symboleAttendu);
  this->avancer();
}

void Interpreteur::erreur(const string & message) throw (SyntaxeException) {
  // Lève une exception contenant le message et le symbole courant trouvé
  // Utilisé lorsqu'il y a plusieurs symboles attendus possibles...
  static char messageWhat[256];
  sprintf(messageWhat,
          "Ligne %d, Colonne %d - Erreur de syntaxe - %s - Symbole trouvé : %s",
          m_lecteur.getLigne(), m_lecteur.getColonne(), message.c_str(), m_lecteur.getSymbole().getChaine().c_str());
  analyseValide = false;
  throw SyntaxeException(messageWhat);
}

//////////////////////////////////////////////////////////////////////////////////////
//      Interpreteur d'instructions
//////////////////////////////////////////////////////////////////////////////////////
NoeudSeqInst *Interpreteur::seqInst() {
// <seqInst> ::= <inst> { <inst> }
    NoeudSeqInst* sequence = new NoeudSeqInst();
    
    do {
        sequence->ajoute(inst());
    } while (m_lecteur.getSymbole() == "<VARIABLE>"
                || m_lecteur.getSymbole() == "si"
                || m_lecteur.getSymbole() == "ecrire"
                || m_lecteur.getSymbole() == "lire"
                || m_lecteur.getSymbole() == "tantque"
                || m_lecteur.getSymbole() == "repeter"
                || m_lecteur.getSymbole() == "pour"
                || m_lecteur.getSymbole() == "selon"
                || m_lecteur.getSymbole() == "appel"
                );
    // Tant que le symbole courant est un début possible d'instruction...
    // Il faut compléter cette condition chaque fois qu'on rajoute une nouvelle instruction
    return sequence;
}

Noeud* Interpreteur::inst() {
    // <inst> ::= <affectation>  ; | <instSi>
    try {
        if (m_lecteur.getSymbole() == "<VARIABLE>") {
          Noeud *affect = affectation();
          testerEtAvancer(";");
          return affect;
        }
        else if (m_lecteur.getSymbole() == "si")
          return instSiRiche();
        // Compléter les alternatives chaque fois qu'on rajoute une nouvelle instruction
        else if (m_lecteur.getSymbole() == "tantque")
            return instTantQue();
        else if (m_lecteur.getSymbole() == "repeter")
            return instRepeter();
        else if (m_lecteur.getSymbole() == "pour")
            return instPour();
        else if (m_lecteur.getSymbole() == "ecrire")
            return instEcrire();
        else if (m_lecteur.getSymbole() == "lire")
            return instLire();
        else if (m_lecteur.getSymbole() == "selon")
            return instSelon();
        else if (m_lecteur.getSymbole() == "appel")
            return instAppel();
        else erreur("Instruction incorrecte");
    } catch (SyntaxeException & e) {
        std::cerr << e.what() << " dans <inst>" << endl;
    }
    return nullptr;
}

NoeudAffectation *Interpreteur::affectation() {
  // <affectation> ::= <variable> = <expression> 
    try {
        Pile &pile = Pile::get();
        NoeudValue *var;
        Noeud *exp;
        tester("<VARIABLE>");
        var = pile[m_lecteur.getSymbole().getChaine()];
        avancer();
        testerEtAvancer("=");
        exp = expression();
        return new NoeudAffectation(var, exp);
    } catch (SyntaxeException & e) {
        std::cerr << e.what() << " dans <affectation>" << endl;
    }
    return nullptr;
}

Noeud* Interpreteur::expression() {
    //<expression> ::= <terme> {+ <terme> |-<terme> }
    Noeud* ter = terme();
    while ( m_lecteur.getSymbole() == "+"  || m_lecteur.getSymbole() == "-") {
      Symbole operateur = m_lecteur.getSymbole(); // On mémorise le symbole de l'opérateur
      avancer();
      Noeud* termeDroit = terme(); // On mémorise le terme droit
      ter = new NoeudOperateurBinaire(operateur, ter, termeDroit); // Et on construit un noeud opérateur binaire
    }
    return ter; // On renvoie ter qui pointe sur la racine de l'expression
}

Noeud* Interpreteur::terme() {
    //<terme>::= <facteur> {*<facteur> |/<facteur> }
    Noeud* fact = facteur();
    while ( m_lecteur.getSymbole() == "*"  || m_lecteur.getSymbole() == "/") {
        Symbole operateur = m_lecteur.getSymbole(); // On mémorise le symbole de l'opérateur
        avancer();
        Noeud* factDroit = facteur(); // On mémorise l'opérande droit
        fact = new NoeudOperateurBinaire(operateur, fact, factDroit); // Et on construuit un noeud opérateur binaire
    }
    return fact; // On renvoie fact qui pointe sur la racine de l'expression
}

Noeud* Interpreteur::facteur() {
    // <facteur> ::= <entier> | <variable> | - <expBool> | non <expBool> | ( <expBool> )
    try {
        Pile &pile = Pile::get();
        Noeud* fact = nullptr;
        
        if (fact = this->creerValeur()) {
            avancer();
        } else if (m_lecteur.getSymbole() == "-") { // - <facteur>
            avancer();
            // on représente le moins unaire (- facteur) par une soustraction binaire (0 - facteur)
            fact = new NoeudOperateurBinaire(Symbole("-"), pile["0"], expBool());
        } else if (m_lecteur.getSymbole() == "non") { // non <facteur>
            avancer();
            // on représente le moins unaire (- facteur) par une soustractin binaire (0 - facteur)
            fact = new NoeudOperateurBinaire(Symbole("non"), expBool(), nullptr);
        } else if (m_lecteur.getSymbole() == "(") { // expression parenthésée
            avancer();
            fact = expBool();
            testerEtAvancer(")");
        } else {
            erreur("Facteur incorrect");
        }
        return fact;
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <facteur>" << endl;
    }
    return nullptr;
}

Noeud* Interpreteur::expBool() {
    //<expBool> ::= <relationET> {ou <relationEt> }
    Noeud* relatEt = relationEt();
    while (m_lecteur.getSymbole() == "ou") {
        avancer();
        Noeud* relatEtDroit = relationEt(); // On mémorise l'opérande droit
        relatEt = new NoeudOperateurBinaire(Symbole("ou"), relatEt, relatEtDroit); // Et on construit un noeud opérateur binaire
    }
    return relatEt;
}

Noeud* Interpreteur::relationEt() {
    //<relationEt> ::= <relation> {et <relation> }
    Noeud* relat = relation();
    while (m_lecteur.getSymbole() == "et") {
        avancer();
        Noeud* relatDroit = relation();
        relat = new NoeudOperateurBinaire(Symbole("et"), relat, relatDroit);
    }
    return relat;
}

Noeud* Interpreteur::relation() {
    //<relation> ::= <expression> { <opRel> <expression> }
    Noeud* exp = expression();
    while (m_lecteur.getSymbole() == "<"  || m_lecteur.getSymbole() == "<=" ||
          m_lecteur.getSymbole() == ">"  || m_lecteur.getSymbole() == ">=" ||
          m_lecteur.getSymbole() == "==" || m_lecteur.getSymbole() == "!=" ) {
        Symbole operateur = m_lecteur.getSymbole(); // On mémorise le symbole de l'opérateur
        avancer();
        Noeud* expDroit = expression();
        exp = new NoeudOperateurBinaire(operateur, exp, expDroit);
    }
    return exp;
}

NoeudInstSiRiche *Interpreteur::instSiRiche(){
    try {
        NoeudInstSiRiche *siRiche = new NoeudInstSiRiche();
        Noeud *condition, *sequence;
        testerEtAvancer("si");
        videInstCond(&condition, &sequence);
        siRiche->ajoute(new NoeudInstSi(condition,sequence));
        while (m_lecteur.getSymbole()=="sinonsi"){
            avancer();
            videInstCond(&condition, &sequence);
            siRiche->ajoute(new NoeudInstSi(condition,sequence));
        }
        if (m_lecteur.getSymbole()=="sinon"){
            avancer();
            siRiche->ajoute(new NoeudInstSi(nullptr,seqInst()));
        }
        testerEtAvancer("finsi");
        return siRiche;
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instSiRiche>" << endl;
    }
    return nullptr;
}

NoeudInstTantQue *Interpreteur::instTantQue(){   
    try {
        Noeud *condition, *sequence;
        testerEtAvancer("tantque");
        videInstCond(&condition, &sequence);
        testerEtAvancer("fintantque");
        return new NoeudInstTantQue(condition,sequence);
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instTantQue>" << endl;
    }
    return nullptr;
}

NoeudInstRepeter *Interpreteur::instRepeter(){
    try {
        testerEtAvancer("repeter");
        Noeud* sequence = seqInst();
        testerEtAvancer("jusqua");
        testerEtAvancer("(");
        Noeud* condition = expBool();
        testerEtAvancer(")");
        return new NoeudInstRepeter(condition,sequence);
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instRepeter>" << endl;
    }
    return nullptr;
}

NoeudInstPour* Interpreteur::instPour(){
    try {
        Noeud *affectation1 = nullptr, *condition, *affectation2 = nullptr, *sequence;
        testerEtAvancer("pour");
        testerEtAvancer("(");
        if (m_lecteur.getSymbole()!=";"){ //savoir s'il y a une affectation en début
            affectation1 = affectation();
        }
        testerEtAvancer(";");
        condition = expBool();
        testerEtAvancer(";");
        if (m_lecteur.getSymbole()!=")"){ //savoir s'il y a une affectation en fin
            affectation2 = affectation();
        }
        testerEtAvancer(")");
        sequence = seqInst();
        testerEtAvancer("finpour");
        return new NoeudInstPour(affectation1, condition, affectation2, sequence);
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instPour>" << endl;
    }
    return nullptr;
}

NoeudInstEcrire* Interpreteur::instEcrire() {
    try {
        NoeudInstEcrire* ecrire = new NoeudInstEcrire();
        
        testerEtAvancer("ecrire");
        tester("(");
        do {
            Noeud *inst;
            avancer();

            if(inst = this->creerValeur()) {
                avancer();
            } else {
                inst = expression();
            }
            ecrire->ajoute(inst);

        } while (m_lecteur.getSymbole()==",");
        testerEtAvancer(")");
        return ecrire;
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instEcrire>" << endl;
    }
    return nullptr;
}

NoeudInstLire* Interpreteur::instLire() {
    try {
        Pile &pile = Pile::get();
        NoeudInstLire* lire = new NoeudInstLire();
        testerEtAvancer("lire");
        tester("(");
        do {       
            avancer();
            tester("<VARIABLE>");
            lire->ajoute(pile[m_lecteur.getSymbole().getChaine()]); // La variable est ajoutée à la table et on la mémorise
            avancer();
        } while (m_lecteur.getSymbole()==",");
        testerEtAvancer(")");
        return lire;
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instLire>" << endl;
    }
    return nullptr;
}

NoeudInstSiRiche* Interpreteur::instSelon() {
    //<instSelon> ::= selon (<variable>) cas <entier> : <seqInst> {cas <entier> : <seqInst> } [defaut : <seqInst>] finselon
    try {
        Pile &pile = Pile::get();
        NoeudInstSiRiche *selon = new NoeudInstSiRiche();
        Noeud *var, *cond, *seq;
        testerEtAvancer("selon");
        testerEtAvancer("(");
        tester("<VARIABLE>");
        var = pile[m_lecteur.getSymbole().getChaine()]; // La variable est ajoutée à la table et on la mémorise
        avancer();
        testerEtAvancer(")");
        
        do {
            testerEtAvancer("cas");
            
            if(cond = this->creerValeur()) {
                avancer();
            } else {
                throw InterpreteurException();
            }
            
            testerEtAvancer(":");
            
            selon->ajoute(new NoeudInstSi(new NoeudOperateurBinaire(Symbole("=="), var, cond),
                                            seqInst()));
        } while (m_lecteur.getSymbole() == "cas");
        if (m_lecteur.getSymbole() == "defaut"){
            avancer();
            testerEtAvancer(":");
            selon->ajoute(new NoeudInstSi(nullptr, seqInst()));
        }
        testerEtAvancer("finselon");
        return selon;        
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instSelon>" << endl;
    }
    return nullptr;
}

NoeudInstAppel* Interpreteur::instAppel() {
    // <appel> ::= appel <variable>([<variable> {, <variable> }]);
    try {
        NoeudInstAppel *appel;
        NoeudProcedure *proc;
        NoeudValue *param;
        
        testerEtAvancer("appel");
        tester("<VARIABLE>");
        proc = this->getProcedure(this->m_lecteur.getSymbole().getChaine());
        
        if(proc == nullptr)
            throw AppelException();
        
        appel = new NoeudInstAppel(proc);
        avancer();
        tester("(");
        
        do {
            avancer();
            if(param = this->creerValeur()) {
                appel->ajoute(param);
                avancer();
            } else {
                throw InterpreteurException();
            }
        } while(this->m_lecteur.getSymbole() == ",");
        testerEtAvancer(")");
        testerEtAvancer(";");

        return appel;
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instAppel>" << endl;
    }
}

NoeudProcedure* Interpreteur::procedure() {
    Pile &pile = Pile::get();
    NoeudProcedure *proc = nullptr;
    try {
        
        testerEtAvancer("procedure");
        tester("<VARIABLE>");
        
        proc = new NoeudProcedure(this->m_lecteur.getSymbole().getChaine());
        pile.empiler(&proc->getTable());
        
        avancer();
        tester("(");
        do {
            avancer();
            if(this->m_lecteur.getSymbole() == "<VARIABLE>") {
                pile[this->m_lecteur.getSymbole().getChaine()];
                avancer();
            }
        } while(this->m_lecteur.getSymbole() == ",");
        testerEtAvancer(")");
        
        proc->ajoute(seqInst());
        testerEtAvancer("finproc");
        
        pile.depiler();
        
        return proc;
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instProcedure>" << endl;
        if(proc != nullptr)
            pile.depiler();
        
        while(this->m_lecteur.getSymbole() != "finproc")
            avancer();
        avancer();
    }
    
    return nullptr;
}

void Interpreteur::videInstCond(Noeud** condition, Noeud** sequence) {
    //permet d'obtenir l'interprétation d'une condition avec une séquence d'instruction après
    try {
        testerEtAvancer("(");
        *condition = expBool();
        testerEtAvancer(")");
        *sequence = seqInst();
    } catch (SyntaxeException &e) {
        std::cerr << e.what() << " dans <instCond>" << endl;
    }
}

NoeudValue *Interpreteur::creerValeur() {
    Pile &pile = Pile::get();
    NoeudValue *noeud = nullptr;

    if (m_lecteur.getSymbole() == "<VARIABLE>") {
        noeud = pile[m_lecteur.getSymbole().getChaine()];
    } else if(m_lecteur.getSymbole() == "<ENTIER>") {
        noeud = pile[":" + m_lecteur.getSymbole().getChaine()];
        *noeud = Valeur(m_lecteur.getSymbole().getChaine());
    } else if(m_lecteur.getSymbole() == "<CHAINE>") {
        std::string chaine = m_lecteur.getSymbole().getChaine();
        chaine.pop_back();
        chaine.erase(0, 1);
        noeud = pile[":" + chaine];
        *noeud = Valeur(chaine);
    }

    return noeud;
}

//////////////////////////////////////////////////////////////////////////////////////
//      Tradution en d'autres languages
//////////////////////////////////////////////////////////////////////////////////////

void Interpreteur::traduitEnCPP(std::ostream & sortie, unsigned int indentation) const {
    sortie << "#include \"ValeurCPP.h\"" << std::endl;
    sortie << "#include <iostream>" << std::endl;
    sortie << "#define et &&" << std::endl;
    sortie << "#define ou ||" << std::endl;
    sortie << std::endl;
    
    for (NoeudProcedure *proc : this->m_procedures) {
        proc->traduitEnCPP(sortie,indentation);
    }
    
    sortie << "int main() {" << std::endl;
    sortie << Indente(indentation+1) << "principale();" << std::endl;
    sortie << Indente(indentation+1) << "return 0;" << std::endl ; 
    sortie << "}" << std::endl ; // Fin d’un programme C++
}
