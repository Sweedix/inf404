#include <stdlib.h>
#include <typeinfo>
#include "ArbreAbstrait.h"
#include "Symbole.h"
#include "Exceptions.h"
#include "Execution.h"

////////////////////////////////////////////////////////////////////////////////
// Indente
////////////////////////////////////////////////////////////////////////////////

Indente::Indente(unsigned int indent) :
indentation(indent) {
}

void Indente::indenter(std::ostream &out) const {
    out << setw(4 * this->indentation) << "";
}

std::ostream &operator <<(std::ostream &out, Indente indent) {
    indent.indenter(out);
}

////////////////////////////////////////////////////////////////////////////////
// Noeud
////////////////////////////////////////////////////////////////////////////////

void Noeud::ajoute(Noeud* instruction) {
    throw OperationInterditeException();
}

Noeud::~Noeud() {
}

////////////////////////////////////////////////////////////////////////////////
// NoeudValue
////////////////////////////////////////////////////////////////////////////////

int NoeudValue::nextId;

NoeudValue::NoeudValue() :
m_id(NoeudValue::nextId++),
m_valeur(),
m_defini(false) {
}

NoeudValue::~NoeudValue() {
}

bool NoeudValue::estDefini() {
    return this->m_defini;
}

void NoeudValue::indefinir() {
    this->m_valeur = Valeur();
    this->m_defini = false;
}

Valeur NoeudValue::executer() {
    return this->m_valeur;
}

void NoeudValue::operator=(Valeur valeur) {
    this->m_defini = true;
    this->m_valeur = valeur;
}

void NoeudValue::traduitEnCPP(std::ostream &sortie, unsigned int indentation) const {
    Pile &pile = Pile::get();
    
    sortie << Indente(indentation) << pile.sommet()->valeurCPP(this);
}

////////////////////////////////////////////////////////////////////////////////
// NoeudSeqInst
////////////////////////////////////////////////////////////////////////////////

NoeudSeqInst::NoeudSeqInst() :
m_instructions() {
}

NoeudSeqInst::~NoeudSeqInst() {
}

Valeur NoeudSeqInst::executer() {
    for(Noeud *noeud : this->m_instructions) {
        noeud->executer();
    }

    return 0;
}

void NoeudSeqInst::ajoute(Noeud* instruction) {
    if(instruction != nullptr)
        this->m_instructions.push_back(instruction);
}

void NoeudSeqInst::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    for(Noeud *noeud : this->m_instructions) {
        noeud->traduitEnCPP(sortie, indentation+1);
        sortie << ";" << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////
// NoeudAffectation
////////////////////////////////////////////////////////////////////////////////

NoeudAffectation::NoeudAffectation(NoeudValue *variable, Noeud *expression) :
m_variable(variable),
m_expression(expression) {
}

NoeudAffectation::~NoeudAffectation() {
}

Valeur NoeudAffectation::executer() {
    *(this->m_variable) = m_expression->executer();
    
    return 0;
}

void NoeudAffectation::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    sortie << Indente(indentation);
    this->m_variable->traduitEnCPP(sortie);
    sortie << " = ";
    this->m_expression->traduitEnCPP(sortie);
}

////////////////////////////////////////////////////////////////////////////////
// NoeudOperateurBinaire
////////////////////////////////////////////////////////////////////////////////

NoeudOperateurBinaire::NoeudOperateurBinaire(Symbole operateur, Noeud* operandeGauche, Noeud* operandeDroit) :
m_operateur(operateur),
m_operandeGauche(operandeGauche),
m_operandeDroit(operandeDroit) {
}

NoeudOperateurBinaire::~NoeudOperateurBinaire() {
}

Valeur NoeudOperateurBinaire::executer() {
    static const Valeur zero(0);
    Valeur og, od, valeur;
    if (m_operandeGauche != nullptr)
        og = m_operandeGauche->executer();

    if (m_operandeDroit != nullptr)
        od = m_operandeDroit->executer();
    
    if(this->m_operateur == "+")
        valeur = (og + od);
    else if (this->m_operateur == "-")
        valeur = (og - od);
    else if (this->m_operateur == "*")
        valeur = (og * od);
    else if (this->m_operateur == "==")
        valeur = (og == od);
    else if (this->m_operateur == "!=")
        valeur = (og != od);
    else if (this->m_operateur == "<")
        valeur = (og < od);
    else if (this->m_operateur == ">")
        valeur = (og > od);
    else if (this->m_operateur == "<=")
        valeur = (og <= od);
    else if (this->m_operateur == ">=")
        valeur = (og >= od);
    else if (this->m_operateur == "et")
        valeur = (og && od);
    else if (this->m_operateur == "ou")
        valeur = (og || od);
    else if (this->m_operateur == "non")
        valeur = (!og);
    else if (this->m_operateur == "/") {
        if (od == zero)
            throw DivParZeroException();
        valeur = og / od;
    }
    
    return valeur;
}

void NoeudOperateurBinaire::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    sortie << Indente(indentation);
    this->m_operandeGauche->traduitEnCPP(sortie);
    sortie << " " << this->m_operateur.getChaine() << " ";
    this->m_operandeDroit->traduitEnCPP(sortie);
}

////////////////////////////////////////////////////////////////////////////////
// NoeudInstSi
////////////////////////////////////////////////////////////////////////////////

NoeudInstSi::NoeudInstSi(Noeud* condition, Noeud* sequence) :
m_condition(condition),
m_sequence(sequence) {
}

NoeudInstSi::~NoeudInstSi() {
}

Valeur NoeudInstSi::executer() {
    if (m_condition == nullptr || m_condition->executer()) { 
        // si condition == nullptr alors pas de condition donc forcément exécutée
        m_sequence->executer();
        return 1;
    }
    return 0; // La valeur renvoyée permet de savoir si la séquence a été executé
}

void NoeudInstSi::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    if(this->m_condition == nullptr) {
        sortie << "{" << std::endl;
    } else {
        sortie << "if ( ";
        this->m_condition->traduitEnCPP(sortie);
        sortie << " ) {" << std::endl;
    }
    this->m_sequence->traduitEnCPP(sortie, indentation);
    sortie << Indente(indentation) << "}" << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
// NoeudInstSiRiche
////////////////////////////////////////////////////////////////////////////////

NoeudInstSiRiche::NoeudInstSiRiche() {
}

NoeudInstSiRiche::~NoeudInstSiRiche() {
}

Valeur NoeudInstSiRiche::executer() {
    for (Noeud* instSi : this->m_instsSi){
        if(instSi->executer())
            break;
    }
    
    return 0;
}

void NoeudInstSiRiche::ajoute(Noeud* instSi){
    if(instSi != nullptr)
        this->m_instsSi.push_back(instSi);
}

void NoeudInstSiRiche::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    int i, length = this->m_instsSi.size();
    
    sortie << Indente(indentation);
    this->m_instsSi[0]->traduitEnCPP(sortie, indentation);
    for(i = 1; i < length; i++) {
        sortie << Indente(indentation) << "else ";
        this->m_instsSi[i]->traduitEnCPP(sortie, indentation);
    }
}

////////////////////////////////////////////////////////////////////////////////
// NoeudInstTantQue
////////////////////////////////////////////////////////////////////////////////

NoeudInstTantQue::NoeudInstTantQue(Noeud* expression, Noeud* sequence):
m_condition(expression),
m_sequence(sequence) {
}

NoeudInstTantQue::~NoeudInstTantQue() {
}

Valeur NoeudInstTantQue::executer() {
    while(this->m_condition->executer()){
        this->m_sequence->executer();
    }

    return 0;
}

void NoeudInstTantQue::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    sortie << Indente(indentation) << "while ( " ;
    this->m_condition->traduitEnCPP(sortie);
    sortie << " ) {" << std::endl;
    this->m_sequence->traduitEnCPP(sortie, indentation);
    sortie << Indente(indentation) << "}" << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
// NoeudInstRepeter
////////////////////////////////////////////////////////////////////////////////

NoeudInstRepeter::NoeudInstRepeter(Noeud* condition, Noeud* sequence) :
m_sequence(sequence),
m_condition(condition) {
}

NoeudInstRepeter::~NoeudInstRepeter() {
}

Valeur NoeudInstRepeter::executer() {
    do {
        this->m_sequence->executer();
    } while (this->m_condition->executer());
    return 0; // La valeur renvoyée ne représente rien !
}


void NoeudInstRepeter::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    sortie << Indente(indentation) << "do {" << std::endl;
    this->m_sequence->traduitEnCPP(sortie, indentation);
    sortie << Indente(indentation) << "} while ( ";
    this->m_condition->traduitEnCPP(sortie, 0);
    sortie << " );" << std::endl;
}


////////////////////////////////////////////////////////////////////////////////
// NoeudInstPour
////////////////////////////////////////////////////////////////////////////////

NoeudInstPour::NoeudInstPour(Noeud *affect1, Noeud* expression, Noeud *affect2, Noeud* sequence) :
m_affect1(affect1),
m_expression(expression),
m_sequence(sequence),
m_affect2(affect2) {
}

NoeudInstPour::~NoeudInstPour() {
}

Valeur NoeudInstPour::executer() {
    if (this->m_affect1 != nullptr)
        this->m_affect1->executer();
    while (this->m_expression->executer()){
        this->m_sequence->executer();
        if (this->m_affect2 != nullptr)
            this->m_affect2->executer();
    }
    
    return 0;
}

void NoeudInstPour::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    sortie << Indente(indentation) << "for ( ";
    if (this->m_affect1!= nullptr) {
        this->m_affect1->traduitEnCPP(sortie, 0);
    }
    sortie << "; ";
    this->m_expression->traduitEnCPP(sortie, 0);
    sortie << " ; ";
    if (this->m_affect2!= nullptr) {
        this->m_affect2->traduitEnCPP(sortie, 0);
    }
    sortie << " ) {" << endl;
    this->m_sequence->traduitEnCPP(sortie, indentation);
    sortie << Indente(indentation) << "}" << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
// NoeudInstEcrire
////////////////////////////////////////////////////////////////////////////////

NoeudInstEcrire::NoeudInstEcrire() {
}

NoeudInstEcrire::~NoeudInstEcrire() {
}

Valeur NoeudInstEcrire::executer() {
    for(Noeud* p : this->m_expressions) {
        cout << p->executer();
    }
    cout << endl;
    return 0;
}

void NoeudInstEcrire::ajoute(Noeud* expression){
    if (expression != nullptr)
        this->m_expressions.push_back(expression);
}

void NoeudInstEcrire::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    sortie << Indente(indentation) << "std::cout ";
    for (Noeud *e : this->m_expressions) {
        sortie << " << ";
        e->traduitEnCPP(sortie, 0);
    }
    sortie << " << std::endl" << " ;" << std::endl;
}


////////////////////////////////////////////////////////////////////////////////
// NoeudInstLire
////////////////////////////////////////////////////////////////////////////////

NoeudInstLire::NoeudInstLire() {
}

NoeudInstLire::~NoeudInstLire() {
}

Valeur NoeudInstLire::executer() {
    Valeur valeur;
    for (Noeud* p : this->m_variables) {
        cin >> valeur;
        *static_cast<NoeudValue *>(p) = valeur;
    }
    
    return 0;
}

void NoeudInstLire::ajoute(Noeud* variable) {
    if (variable != nullptr)
        this->m_variables.push_back(variable);
}

void NoeudInstLire::traduitEnCPP(std::ostream& sortie, unsigned int indentation) const {
    sortie << Indente(indentation) << "std::cin ";
    for (Noeud *v : this->m_variables) {
        sortie << " >> ";
        v->traduitEnCPP(sortie, 0);
    }
    sortie << " ;" << std::endl;
}
