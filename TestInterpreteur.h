/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   TestInterpreteur.h
 * Author: valentin
 *
 * Created on 6 nov. 2017, 15:43:03
 */

#ifndef TESTINTERPRETEUR_H
#define TESTINTERPRETEUR_H

#include <cppunit/extensions/HelperMacros.h>

class TestInterpreteur : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestInterpreteur);

    CPPUNIT_TEST(testAnalyse);
    CPPUNIT_TEST(testExecuter);

    CPPUNIT_TEST_SUITE_END();

public:
    TestInterpreteur();
    virtual ~TestInterpreteur();
    void setUp();
    void tearDown();

private:
    void testAnalyse();
    void testExecuter();

};

#endif /* TESTINTERPRETEUR_H */

