#ifndef INTERPRETEUR_H
#define INTERPRETEUR_H

#include "Lecteur.h"
#include "Exceptions.h"
#include "ArbreAbstrait.h"
#include "Execution.h"
#include <vector>

class Interpreteur {
public:
	Interpreteur(ifstream &fichier);
        ~Interpreteur();

	void analyse() throw (InterpreteurException);
        /*  <analyse> ::= { <procedure> } <FINDEFICHIER>
         *  Si le contenu du fichier est conforme à la grammaire,
         * cette méthode se termine normalement et affiche un message "Syntaxe correcte".
         * la table des symboles (ts) et l'arbre abstrait (arbre) auront été construits
         * Sinon, une exception sera levée
         */

        
        Table &executer(const std::string &named) throw (InterpreteurException);
        void traduitEnCPP(ostream & sortie, unsigned int indentation =0) const;	
private:
    Lecteur        m_lecteur;
    bool           analyseValide;
    std::vector<NoeudProcedure *> m_procedures;
    NoeudProcedure *getProcedure(const std::string &named);
    
    // Implémentation de la grammaire
    NoeudProcedure     *procedure();    // <procédure> ::= procedure <chaine> ([<variable> {, <variable> }]) <seqInst> finproc
    NoeudSeqInst       *seqInst();      // <seqInst> ::= <inst> { <inst> }
    Noeud              *inst();         // <inst> ::= <affectation> ; | <instSi>
    NoeudAffectation   *affectation();  // <affectation> ::= <variable> = <expression> 
    Noeud              *expression();   // <expression> ::= <terme> {+ <terme> |-<terme> }
    Noeud              *terme();        // <terme>::= <facteur> {*<facteur> |/<facteur> }
    Noeud              *facteur();      // <facteur> ::= <entier>  |  <variable>  |  - <facteur>  | non <facteur> | ( <expression> )
    Noeud              *expBool();      // <expBool> ::= <relationET> {ou <relationEt> }
    Noeud              *relationEt();   // <relationEt> ::= <relation> {et <relation> }
    Noeud              *relation();     // <relation> ::= <expression> { <opRel> <expression> }
    NoeudInstSiRiche   *instSiRiche();  // <instSiRiche> ::= si (<expBool>) <seqInst> {sinonsi(<expression>) <seqInst> }[sinon <seqInst>]finsi
    NoeudInstTantQue   *instTantQue();  // <instTantQue> ::=tantque( <expBool> ) <seqInst> fintantque
    NoeudInstRepeter   *instRepeter();  // <instRepeter>::=repeter <seqInst> jusqua( <expBool> )
    NoeudInstPour      *instPour();     // <instPour> ::=pour( [ <affectation> ] ; <expBool> ;[ <affectation> ]) <seqInst> finpour
    NoeudInstEcrire    *instEcrire();   // <instEcrire>  ::=ecrire( <expression> | <chaine> {, <expression> | <chaine> })    
    NoeudInstLire      *instLire();     // <instLire> ::=lire( <variable> {, <variable> })
    NoeudInstSiRiche   *instSelon();    // <instSelon> ::= selon (<variable>) {cas <entier> : <seqInst> } [defaut : <seqInst>] finselon
    NoeudInstAppel     *instAppel();    // <appel> ::= appel <chaine>([<variable> {, <variable> }]);
    
    // outils pour simplifier l'analyse lexical
    
    void videInstCond(Noeud** condition, Noeud** sequence);
    NoeudValue         *creerValeur();
    
    // outils pour simplifier l'analyse syntaxique

    void tester (const string & symboleAttendu) throw (SyntaxeException);
    // Si symbole courant != symboleAttendu, on lève une exception
    void avancer();
    void testerEtAvancer(const string & symboleAttendu) throw (SyntaxeException);
    // Si symbole courant != symboleAttendu, on lève une exception, sinon on avance
    void erreur (const string & mess) throw (SyntaxeException);
    // Lève une exception "contenant" le message mess
};

#endif /* INTERPRETEUR_H */
