/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <iostream>
#include <cstdlib>

#include "Exceptions.h"
#include "Execution.h"


////////////////////////////////////////////////////////////////////////////////
//  Table
////////////////////////////////////////////////////////////////////////////////

Table::Table(const std::string &nom) :
m_nom(nom),
m_table() {
}

NoeudValue &Table::operator[](const std::string &nom) {
    /*
     *  Si un objet n'existe pas dans une map, map le cree et se l'ajoute
     * si il a un constructeur par défaut, ce qui est le cas de NoeudValue
     */
    return this->m_table[nom];
}

void Table::definir(std::vector<Noeud *> parametres) {
    unsigned long i = 0;
    
    for(auto &paire : this->m_table) {
        if(paire.first.front() != ':') {
            if(i == parametres.size()) 
            // vérifie qu'il n'y a pas plus d'arguments que de paramètres
                throw ParametresException();
            (*this)[paire.first] = parametres[i]->executer();
            i++;
        }
    }
    if(i != parametres.size())
        // vérifie qu'il n'y a pas plus de paramètres que d'arguments
        throw ParametresException();
}
    
void Table::indefinir() {
    for(auto &paire : this->m_table) {
        if(paire.first.front() != ':') {
            paire.second.indefinir();
        }
    }
}

const std::string &Table::getNom() const {
    return this->m_nom;
}

void Table::afficher() {
    std::cout << "Affichant table " << this->m_nom << std::endl;
    for(auto &paire : this->m_table) {
        if(paire.second.estDefini())
            std::cout << "\tVariable " << paire.first << " avec valeur: " << paire.second.executer() << std::endl;
        else
            std::cout << "\tVariable " << paire.first << " indéfinie" << std::endl;
    }
    
    std::cout << std::endl;
}

std::string Table::parametresCPP() const {
    std::string parametres;
    
    for(auto &paire : this->m_table) {
        if(paire.first.front() != ':') {
            parametres += "ValeurCPP " + paire.first + " = 0, ";
        }
    }
    
    parametres.erase(parametres.size()-2);
    
    return parametres;
}

const std::string Table::valeurCPP(const NoeudValue *valeur) const {
    
    for(auto &paire : this->m_table) {
        if(paire.second.m_id == valeur->m_id) {
            if(paire.first.front() == ':') {
                return "ValeurCPP(\"" + valeur->m_valeur.m_valeur + "\")";
            } else {
                return paire.first;
            }
        }
    }
    
    throw exception();
}

////////////////////////////////////////////////////////////////////////////////
//  Pile
////////////////////////////////////////////////////////////////////////////////

Pile *Pile::instance;

Pile::Pile() {
}

void Pile::deinit() {
    delete Pile::instance;
}

Pile &Pile::get() {
    if(Pile::instance == nullptr) {
        Pile::instance = new Pile();
        atexit(Pile::deinit);
        // Permet de bien libérer le singleton si l'execution finit proprement
    }
    
    return *Pile::instance;
}

Table *Pile::sommet() {
    if(this->m_stack.empty())
        return nullptr;
    return this->m_stack.top();
}

void Pile::empiler(Table *table) {
    this->m_stack.push(table);
}

void Pile::depiler() {
    if(this->m_stack.empty())
        std::cerr << "Pile vide" << endl;
    else
        this->m_stack.pop();
}

NoeudValue *Pile::operator[](const std::string &nom) {
    return &(*this->sommet())[nom];
}

////////////////////////////////////////////////////////////////////////////////
// NoeudProcedure
////////////////////////////////////////////////////////////////////////////////

NoeudProcedure::NoeudProcedure(const std::string &nom) :
m_table(nom),
m_sequence(nullptr) {
}

NoeudProcedure::~NoeudProcedure() {
}

void NoeudProcedure::ajoute(Noeud* seq) {
    this->m_sequence = static_cast<NoeudSeqInst *>(seq);
}

Valeur NoeudProcedure::executer() {
    return this->m_sequence->executer();
}

void NoeudProcedure::traduitEnCPP(std::ostream & sortie, unsigned int indentation) const {
    Pile &pile = Pile::get();
    
    sortie << Indente(indentation) << "void " << this->getNom()
            << "(" << this->m_table.parametresCPP() << ") {" << std::endl;
    
    pile.empiler((Table*)(void*)&this->m_table);
    
    this->m_sequence->traduitEnCPP(sortie, indentation);
    
    pile.depiler();
    
    sortie << Indente(indentation) << "}" << std::endl << std::endl;
}

const std::string &NoeudProcedure::getNom() const {
    return this->m_table.getNom();
}

Table &NoeudProcedure::getTable() {
    return this->m_table;
}

////////////////////////////////////////////////////////////////////////////////
// NoeudInstAppel
////////////////////////////////////////////////////////////////////////////////

NoeudInstAppel::NoeudInstAppel(NoeudProcedure *proc) :
m_proc(proc),
m_parametres() {
}

NoeudInstAppel::~NoeudInstAppel() {
}

void NoeudInstAppel::ajoute(Noeud* param) {
    this->m_parametres.push_back(param);
}

Valeur NoeudInstAppel::executer() {
    Pile &pile = Pile::get();
    Table &table = this->m_proc->getTable();
    
    table.definir(this->m_parametres);
    pile.empiler(&table);
    
    this->m_proc->executer();
    
    pile.depiler();
    table.indefinir();
    
    return 0;
}

void NoeudInstAppel::traduitEnCPP(ostream & sortie, unsigned int indentation) const {
    sortie << Indente(indentation) << this->m_proc->getNom() << "( ";
    std::string params;
    
    for (int i = 0; i < this->m_parametres.size(); i++) {
        this->m_parametres[i]->traduitEnCPP(sortie, 0);
        if(i+1 != this->m_parametres.size())
            sortie << ", ";
    }
    
    sortie << " );" << endl;
}
