/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ValeurCPP.cpp
 * Author: valentin
 * 
 * Created on 5 novembre 2017, 21:01
 */

#include "ValeurCPP.h"

ValeurCPP::ValeurCPP(std::string valeur) :
m_valeur(valeur) {
}

ValeurCPP::ValeurCPP(int valeur) :
m_valeur(std::to_string(valeur)) {
}

void ValeurCPP::operator =(ValeurCPP valeur) {
    this->m_valeur = valeur.m_valeur;
}

bool ValeurCPP::operator ==(ValeurCPP valeur) {
    return this->m_valeur == valeur.m_valeur;
}

bool ValeurCPP::operator !=(ValeurCPP valeur) {
    return this->m_valeur != valeur.m_valeur;
}

bool ValeurCPP::operator <=(ValeurCPP valeur) {
    return int(*this) <= int(valeur);
}

bool ValeurCPP::operator >=(ValeurCPP valeur) {
    return int(*this) >= int(valeur);
}

bool ValeurCPP::operator <(ValeurCPP valeur) {
    return int(*this) < int(valeur);
}

bool ValeurCPP::operator >(ValeurCPP valeur) {
    return int(*this) > int(valeur);
}

bool ValeurCPP::operator &&(ValeurCPP valeur) {
    return int(*this) && int(valeur);
}

bool ValeurCPP::operator ||(ValeurCPP valeur) {
    return int(*this) || int(valeur);
}

ValeurCPP ValeurCPP::operator +(ValeurCPP valeur) {
    try {
        return int(*this) + int(valeur);
    } catch(ValeurCPPException &e) {
        return this->m_valeur + valeur.m_valeur;
    }
}

ValeurCPP ValeurCPP::operator -(ValeurCPP valeur) {
    return int(*this) - int(valeur);
}

ValeurCPP ValeurCPP::operator *(ValeurCPP valeur) {
    return int(*this) * int(valeur);
}

ValeurCPP ValeurCPP::operator /(ValeurCPP valeur) {
    return int(*this) / int(valeur);
}

ValeurCPP::operator int() throw (ValeurCPPException) {
    try {
        return std::stoi(this->m_valeur);
    } catch(...) {
        throw ValeurCPPException();
    }
}

std::ostream &operator <<(std::ostream &out, const ValeurCPP &valeur) {
    return out << valeur.m_valeur;
}

std::istream &operator >>(std::istream &input, ValeurCPP &valeur) {
    return input >> valeur.m_valeur;
}